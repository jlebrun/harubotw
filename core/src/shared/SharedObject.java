package shared;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import character.Heros;
import game.DayTime;
import game.GameInput;
import map.GameMap;
import menu.Dialbox;
import menu.Hud;
import player.PlayerBag;
import player.PlayerStat;
import view.GameCamera;

public class SharedObject {

	public GameMap currentMap;
	public Heros heros;
	public GameCamera camera;
	public SpriteBatch spriteBatch;
	public SpriteBatch hudBatch;
	public OrthogonalTiledMapRenderer renderer;
	
	public Dialbox dialbox;
	public BitmapFont font;
	public Hud hud;
	public GameInput input;
	public OrthographicCamera cameraHud;
	public PlayerBag bag;
	public DayTime daytime;
	public PlayerStat herosStat;

	
	public boolean DEBUG_MAP = false;
	public boolean DEBUG_GHOST = false;
	public boolean HEROS_ON_BRIDGE = false;
	public boolean ON_MENU = false;
	public boolean ON_ANDROID = true;
	
	public SharedObject(){
		
	}
	
	
	
	
}
