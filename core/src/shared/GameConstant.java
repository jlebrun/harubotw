package shared;

public class GameConstant {
	
	public static final int WIDTH = 640;
	public static final int HEIGHT = 360;
	public static final int CELL_WIDTH = 32;
	public static final int FRAME_DURATION = 8;
	public static final boolean DEBUG = true;
	public static boolean ANDROID = false;
	public static final String TITLE = "Survive";
}
