package shared;

import java.util.List;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ItemProperty {

	private TextureRegion menuTexture;
	private List<TextureRegion> mapTextures;
	private String name;
	private String code;
	private String family;
	private String description;
	
	/**
	 * Constructeur
	 * @param code
	 * @param menuTexture
	 * @param mapTexture
	 * @param name
	 * @param family
	 * @param description
	 */
	public ItemProperty(String code,String family, TextureRegion menuTexture, List<TextureRegion> mapTextures, String name, String description ){
		this.code = code;
		this.family = family;
		this.name = name;
		this.description = description;
		
		this.menuTexture = menuTexture;
		this.mapTextures = mapTextures;
	}
	
	
	public TextureRegion getMenuTexture() {
		return menuTexture;
	}
	public void setMenuTexture(TextureRegion menuTexture) {
		this.menuTexture = menuTexture;
	}
	public List<TextureRegion> getMapTextures() {
		return mapTextures;
	}
	public void setMapTextures(List<TextureRegion> mapTextures) {
		this.mapTextures = mapTextures;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getFamily() {
		return family;
	}


	public void setFamily(String family) {
		this.family = family;
	}


	public TextureRegion getMapTextureRegion(int index){
		if(index>mapTextures.size()-1 && mapTextures.get(index)!=null){
			return mapTextures.get(0);
		}else{
			return mapTextures.get(index);
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((family == null) ? 0 : family.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemProperty other = (ItemProperty) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (family == null) {
			if (other.family != null)
				return false;
		} else if (!family.equals(other.family))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
