package shared;

import java.util.HashMap;

public class GameItem {
	
	public static final String FAMILY_MUSHROOM = "FAMILY_MUSHROOM"; // famille champignon
	public static final String FAMILY_FLOWER = "FAMILY_FLOWER"; // famille fleur
	
	
	public static final String EQUIP_LEG_PANTS_01 = "LEG_PANTS_01";
	
	public static HashMap<String, ItemProperty> ITEMS_PROPERTIES = new HashMap<String, ItemProperty>();
}
