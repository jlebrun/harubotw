package shared;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GameTexture {

	// STRINGS //////
	public static final String STR_HEROS_WALK = "heros/heros_walk.png";
	
	// TEXTURES /////
	public final static Texture TX_DIALBOX_9 = new Texture("hud/dialbox.9.png");
	public final static Texture TX_DIALBOX_ARROW = new Texture("hud/dialboxArrow.png");
	public final static TextureRegion[][] TXR_HUD_ICONS = TextureRegion.split(new Texture("hud/icons.png"),24,24);
	public final static TextureRegion[][] TXR_DOOR = TextureRegion.split(new Texture("mapItems/animated/door.png"),32,32);

	
	public final static Texture TX_MAP_ITEMS_FOREST = new Texture("mapItems/lootable/forest.png");
	
	// EQUIPEMENTS ////
	public static TextureRegion[][] TXR_EQUIP_LEG_PANTS_01 = TextureRegion.split(new Texture("equipment/legs/pants01.png"),24,44);
	
	
	/**
	 * @param textureRegion la grille de sprites
	 * @param nbFrames le nombre de frame
	 * @param line l'index de la ligne
	 * @param col l'index de la colonne
	 * @param frameDuration la dur�e de chaque frame
	 * @return l'objet animation cr��
	 */
	public static Animation loadAnimation(TextureRegion[][] textureRegion, int nbFrames, int line, float frameDuration){
		TextureRegion[] frames = new TextureRegion[nbFrames];
		for (int i = 0; i < nbFrames; i++) {
			frames[i] = textureRegion[line][i];
		}
		return new Animation(frameDuration, frames);
	}
	
}
