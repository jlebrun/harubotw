package game;

import java.util.Arrays;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import character.Heros;
import map.GameMapImpl;
import menu.Dialbox;
import menu.Hud;
import player.PlayerBag;
import player.PlayerStat;
import shared.GameConstant;
import shared.GameItem;
import shared.GameTexture;
import shared.ItemProperty;
import shared.SharedObject;
import view.GameCamera;

public class Launcher extends ApplicationAdapter {
	
	private SharedObject shared;

	@Override
	public void create () {
		shared = new SharedObject();
		shared.camera = new GameCamera();
		shared.spriteBatch = new SpriteBatch();
		OrthographicCamera camera = new OrthographicCamera(GameConstant.WIDTH, GameConstant.HEIGHT);
		shared.spriteBatch.setProjectionMatrix(camera.combined);
		
		shared.hudBatch = new SpriteBatch();
		shared.cameraHud = new OrthographicCamera(GameConstant.WIDTH, GameConstant.HEIGHT);
		shared.cameraHud.position.set((GameConstant.WIDTH/2), (GameConstant.HEIGHT/2), 0);
		shared.cameraHud.update();
		shared.hudBatch.setProjectionMatrix(shared.cameraHud.combined);
		
		this.initItemProperty();

		shared.dialbox = new Dialbox();
		shared.heros = new Heros(shared);
		shared.font = new BitmapFont();
		shared.font.setColor(Color.WHITE);
		shared.currentMap = new GameMapImpl(shared,"center_0_0.tmx");
		shared.hud = new Hud();
		shared.input = new GameInput(shared);
		shared.bag = new PlayerBag();
		shared.daytime = new DayTime();
		shared.herosStat = new PlayerStat(100,100,5);
	
	}

	@Override
	public void render () {
		this.update();
		
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		shared.camera.render(shared);
		
		
	}
	
	private void update(){
		int dx = 0;
		int dy = 0;
		
		if(!shared.ON_MENU){
			if(shared.input.isKeyUpPressed()){
				dy+=2;
			}
			if(shared.input.isKeyDownPressed()){
				dy-=2;
			}
			if(shared.input.isKeyRightPressed()){
				dx+=2;
			}
			if(shared.input.isKeyLeftPressed()){
				dx-=2;
			}
		}
		
		if(dx != 0 && dy !=0){
			dx *=0.75;
			dy *=0.75;
		}
		
		
		if(dx != 0 || dy != 0){
			shared.heros.translate(shared,dx,dy);
		}else{
			shared.heros.setMove(false);
		}
		
		if(Gdx.input.isKeyPressed(Keys.A)){
			shared.DEBUG_GHOST=true;
		}else if(shared.DEBUG_GHOST){
			shared.DEBUG_GHOST=false;
		}
		
		if(Gdx.input.isKeyPressed(Keys.Z)){
			shared.DEBUG_MAP=true;
		}else if(shared.DEBUG_MAP){
			shared.DEBUG_MAP=false;
		}
		
		shared.currentMap.updateMap(shared);
		shared.heros.update(shared);
		shared.dialbox.update(shared);
		shared.hud.update(shared);
		
		
	}
	
	/**
	 * initialise les propri�t�
	 */
	private void initItemProperty(){
		TextureRegion[][] txItems = TextureRegion.split(GameTexture.TX_MAP_ITEMS_FOREST, 32, 32);
		ItemProperty item;
		item = new ItemProperty("MUSHROOM_RUSSULE",GameItem.FAMILY_MUSHROOM, txItems[0][0], Arrays.asList(txItems[0][1],txItems[0][2],txItems[0][3],txItems[0][4]), "Russule", "Un champignon commun, commestible mais tr�s fade et peu nourrisant.");
		GameItem.ITEMS_PROPERTIES.put(item.getCode(), item);
		item = new ItemProperty("PURPLE_PEONY",GameItem.FAMILY_FLOWER, txItems[1][0], Arrays.asList(txItems[1][1],txItems[1][2],txItems[1][3],txItems[1][4]), "Pivoine violette", "Une jolie fleur violette.");
		GameItem.ITEMS_PROPERTIES.put(item.getCode(), item);
		item = new ItemProperty("WHITE_PEONY",GameItem.FAMILY_FLOWER, txItems[2][0], Arrays.asList(txItems[2][1],txItems[2][2],txItems[2][3],txItems[2][4]), "Pivoine blanche", "Une jolie fleur blanche.");
		GameItem.ITEMS_PROPERTIES.put(item.getCode(), item);
	}
}
