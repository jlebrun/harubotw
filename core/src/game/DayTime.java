package game;

import com.badlogic.gdx.graphics.Texture;

import shared.SharedObject;

public class DayTime {

	private int day = 1;
	private int hour = 15;
	private int cpt;
	private float targetR = 1f;
	private float targetG = 1f;
	private float targetB = 1f;
	private float targetA = 0f;
	
	private float currentR = 1f;
	private float currentG = 1f;
	private float currentB = 1f;
	private float currentA = 0f;
	
	private Texture fogTest = new Texture("maps/center_0_0.png");
	
	/**
	 * Dessine sur le HUD
	 * @param shared
	 */
	public void render(SharedObject shared){
//		if(currentA > 0f){
//			shared.hudBatch.setColor(currentR, currentG, currentB, currentA);
//			shared.hudBatch.draw(fogTest, -shared.heros.getX()+320,-shared.heros.getY()+176);
//			shared.hudBatch.setColor(1, 1, 1, 1f);
//		}
		shared.font.draw(shared.hudBatch, "Day "+day+ " | "+hour+"h", 500, 12);
	}
	
	/**
	 * Met a jour
	 * @param shared
	 */
	public void update(SharedObject shared){
		if(cpt>=240){
			hour++;
			this.switchGameColor(shared);
			this.updatePlayerStat(shared);
			if(hour>24){
				hour = 1;
				day++;
			}
			cpt=0;
		}else{
			cpt++;
		}
		
		if(currentR != targetR || currentG != targetG || currentB != targetB || currentA != targetA){
			this.switchAllBatchColor(shared);
		}
	}
	
	private void switchGameColor(SharedObject shared){
		if(hour>8 && hour<19){
			this.targetA=0f;
		}else if(hour > 21 || hour < 5){
			this.targetA=1f;
		}else if(hour == 7 || hour == 8){
			this.targetA=0.2f;
		}else if(hour == 6){
			this.targetA=0.4f;
		}else if(hour == 5){
			this.targetA=0.8f;
		}else if(hour == 19){
			this.targetA=0.2f;
		}else if(hour == 20){
			this.targetA=0.4f;
		}else if(hour == 21){
			this.targetA=0.7f;
		}
	}
	
	private void switchAllBatchColor(SharedObject shared){
		if(targetR > currentR){
			currentR+=0.01f;
		}else if(targetR < currentR){
			currentR-=0.01f;
		}
		
		if(targetG > currentG){
			currentG+=0.01f;
		}else if(targetG < currentG){
			currentG-=0.01f;
		}
		
		if(targetB > currentB){
			currentB+=0.01f;
		}else if(targetB < currentB){
			currentB-=0.01f;
		}
		
		if(targetA > currentA){
			currentA+=0.002f;
			if(currentA>1f){
				currentA = 1f;
			}
		}else if(targetA < currentA){
			currentA-=0.002f;
		}
		
//		shared.spriteBatch.setColor(currentR, currentG, currentB, 1f);
//		shared.renderer.getBatch().setColor(currentR, currentG, currentB, 1f);

	}
	
	private void updatePlayerStat(SharedObject shared){
		shared.herosStat.setThirst(shared.herosStat.getThirst()-4);
		shared.herosStat.setHunger(shared.herosStat.getHunger()-2);
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}
	
	
}
