package game;

import java.util.HashMap;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.math.Vector3;

import shared.GameConstant;
import shared.SharedObject;

public class GameInput implements ApplicationListener, InputProcessor{

	private  Controller controller360;
	
	private boolean padButtonAPressed = false;
	private boolean padButtonBPressed = false;
	private boolean padButtonStartPressed = false;
	private boolean padButtonLeftPressed = false;
	private boolean padButtonRightPressed = false;
	private boolean padButtonUpPressed = false;
	private boolean padButtonDownPressed = false;
	
	private int timerDown = 0;
	private int timerUp = 0;
	private int timerLeft = 0;
	private int timerRight = 0;
	private int timerStart = 0;
	private int timerSelect = 0;
	
	private static final float PAD_SENSIBILITY = 0.3f;
	private static final int PAD_ID = 0;
	private static final int MAX_TIMER = 10;
	
	private HashMap<Integer, TouchInfo> touches;
	
	private static final int MAX_TOUCH_POINT = 4;
	private float androidRatio;
	private SharedObject shared;
	
	class TouchInfo {
        public float touchX = 0;
        public float touchY = 0;
        public boolean touched = false;
    }
	
	/**
	 * Constructeur
	 */
	public GameInput(SharedObject shared){
		this.shared = shared;
		if(Controllers.getControllers().size > 0){
			for(Controller controller : Controllers.getControllers()){
				if (controller.getName().toLowerCase().contains("xbox")){
					controller360 = Controllers.getControllers().first();
					break;
				}
			}
        }
		Gdx.input.setInputProcessor(this);
		touches = new HashMap<Integer,TouchInfo>();
		for(int i = 0; i < MAX_TOUCH_POINT; i++){
            touches.put(i, new TouchInfo());
        }
		this.androidRatio = (Gdx.graphics.getWidth()/GameConstant.WIDTH);
	}
	
	/**
	 * 
	 * @return vrai si la touche gauche est pressée
	 */
	public boolean isKeyLeftPressed(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.LEFT);
		if (!result && controller360 != null) {
				result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_LEFT ||
				controller360.getAxis(XBox360Pad.AXIS_LEFT_X) < -PAD_SENSIBILITY;
		}
		
		// touch android
        if(isTouch(8,64,48)){
        	result = true;
        }
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche gauche vient d'être pressée
	 */
	public boolean isKeyLeftJustPressed(){
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.LEFT);
		if(!result && controller360 != null){
			if(controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_LEFT ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_X) < -PAD_SENSIBILITY) {
				if(!padButtonLeftPressed){
					result = true;
					padButtonLeftPressed = true;
				}
			}else{
				padButtonLeftPressed = false;
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche droite est pressée
	 */
	public boolean isKeyRightPressed(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.RIGHT);
		if(!result && controller360 != null){
			result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_RIGHT ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_X) > PAD_SENSIBILITY;
		}
		
		// touch android
        if(isTouch(128,64,64)){
        	result = true;
        }
		return result;
	}
	/**
	 * 
	 * @return vrai si la touche droite vient d'être pressée
	 */
	public boolean isKeyRightJustPressed(){
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.RIGHT);
		if(!result && controller360 != null){
			if(controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_RIGHT ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_X) > PAD_SENSIBILITY){
				if(!padButtonRightPressed){
					result = true;
					padButtonRightPressed = true;
				}
			}else{
				padButtonRightPressed = false;
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche bas est pressée
	 */
	public boolean isKeyDownPressed(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.DOWN);
		if(!result && controller360 != null){
			result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_DOWN ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_Y) > PAD_SENSIBILITY;
		}
		
		if(isTouch(64,8,64)){
        	result = true;
        }
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche bas vient d'être pressée
	 */
	public boolean isKeyDownJustPressed(){
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.DOWN);
		if(!result && controller360 != null){
			if(controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_DOWN ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_Y) > PAD_SENSIBILITY){
				if(!padButtonDownPressed){
					result = true;
					padButtonDownPressed = true;
				}
			}else{
				padButtonDownPressed = false;
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche haut est pressée
	 */
	public boolean isKeyUpPressed(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.UP);
		if(!result && controller360 != null){
			result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_UP ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_Y) < -PAD_SENSIBILITY;
		}
		
		// touch android
        if(isTouch(64,128,64)){
        	result = true;
        }
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche haut vient d'être pressée
	 */
	public boolean isKeyUpJustPressed(){
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.UP);
		if(!result && controller360 != null){
			if(controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_UP ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_Y) < -PAD_SENSIBILITY){
				if(!padButtonUpPressed){
					result = true;
					padButtonUpPressed = true;
				}
			}else{
				padButtonUpPressed = false;
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche action1 est pressée
	 */
	public boolean isKeyAction1Pressed(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.SPACE);
		if(!result && controller360 != null){
			result = controller360.getAxis(XBox360Pad.AXIS_RIGHT_TRIGGER) < 0;
		}
		
		// touch android
        if(isTouch(536,88,76)){
        	result = true;
        }
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche action2 est pressée
	 */
	public boolean isKeyAction2Pressed(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.ALT_RIGHT) || Gdx.input.isKeyPressed(Keys.ALT_LEFT);
		if(!result && controller360 != null){
			result = controller360.getButton(XBox360Pad.BUTTON_A);
		}
		
		// touch android
        if(isTouch(472,16,76)){
        	result = true;
        }
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche echap ou start est pressée
	 */
	public boolean isKeyEscapePressed() {
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.ESCAPE);
		if(!result && controller360 != null){
			result = controller360.getButton(XBox360Pad.BUTTON_START);
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche action1 vient d'être pressée
	 */
	public boolean isKeyAction1JustPressed(){
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.SPACE);
		if(!result && controller360 != null && !shared.ON_ANDROID ){
			if(controller360.getAxis(XBox360Pad.AXIS_RIGHT_TRIGGER) < 0){
				if(!padButtonAPressed){
					result = true;
					padButtonAPressed = true;
				}
			}else{
				padButtonAPressed = false;
			}
		}
		
		// touch android
        if(shared.ON_ANDROID && !result && isTouch(536,88,76)){
        	if(!padButtonAPressed){
        		padButtonAPressed = true;
        		result = true;
        	}
        }else{
        	padButtonAPressed = false;
        }
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche action2 vient d'être pressée
	 */
	public boolean isKeyAction2JustPressed(){
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.ALT_RIGHT) || Gdx.input.isKeyJustPressed(Keys.ALT_LEFT);
		if(!result && controller360 != null && !shared.ON_ANDROID){
			if(controller360.getButton(XBox360Pad.BUTTON_A)){
				if(!padButtonBPressed){
					result = controller360.getButton(XBox360Pad.BUTTON_A);
					padButtonBPressed = true;
				}
			}else{
				padButtonBPressed = false;
			}
		}
		
		// touch android
        if(shared.ON_ANDROID && !result && isTouch(472,16,76)){
        	if(!padButtonBPressed){
        		padButtonBPressed = true;
        		result = true;
        	}
        }else{
        	padButtonBPressed = false;
        }
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche echap ou start vient d'être pressée
	 */
	public boolean isKeyEscapeJustPressed(){
		boolean result = false;
		result = Gdx.input.isKeyJustPressed(Keys.ESCAPE);
		if(!result && controller360 != null){
			if(controller360.getButton(XBox360Pad.BUTTON_START)){
				if(!padButtonStartPressed){
					result = controller360.getButton(XBox360Pad.BUTTON_START);
					padButtonStartPressed = true;
				}
			}else{
				padButtonStartPressed = false;
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche bas est pressée (seulement une fois toutes les XX frames)
	 */
	public boolean isKeyDownJustPressedSinceTimer(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.DOWN);
		if(!result && controller360 != null){
			result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_DOWN ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_Y) > PAD_SENSIBILITY;
		}
		
		if(!result){
			// touche non enfoncée, on remet le timer à 0
			timerDown = 0;
			
		}else if(timerDown > 0){
			// touche deja enfoncée mais timer pas fini
			result = false;
			timerDown--;
		}else{
			// premier appui, on init le timer
			timerDown = MAX_TIMER;
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche haut est pressée (seulement une fois toutes les XX frames)
	 */
	public boolean isKeyUpJustPressedSinceTimer(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.UP);
		if(!result && controller360 != null){
			result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_UP ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_Y) < -PAD_SENSIBILITY;
		}
		
		if(!result){
			// touche non enfoncée, on remet le timer à 0
			timerUp = 0;
			
		}else if(timerUp > 0){
			// touche deja enfoncée mais timer pas fini
			result = false;
			timerUp--;
		}else{
			// premier appui, on init le timer
			timerUp = MAX_TIMER;
		}
		
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche gauche est pressée (seulement une fois toutes les XX frames)
	 */
	public boolean isKeyLeftJustPressedSinceTimer(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.LEFT);
		if(!result && controller360 != null){
			result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_LEFT ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_X) < -PAD_SENSIBILITY;
		}
		
		if(!result){
			// touche non enfoncée, on remet le timer à 0
			timerLeft = 0;
			
		}else if(timerLeft > 0){
			// touche deja enfoncée mais timer pas fini
			result = false;
			timerLeft--;
		}else{
			// premier appui, on init le timer
			timerLeft = MAX_TIMER;
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche droite est pressée (seulement une fois toutes les XX frames)
	 */
	public boolean isKeyRightJustPressedSinceTimer(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.RIGHT);
		if(!result && controller360 != null){
			result = controller360.getPov(PAD_ID) == XBox360Pad.BUTTON_DPAD_RIGHT ||
					controller360.getAxis(XBox360Pad.AXIS_LEFT_X) > PAD_SENSIBILITY;
		}
		
		if(!result){
			// touche non enfoncée, on remet le timer à 0
			timerRight = 0;
			
		}else if(timerRight > 0){
			// touche deja enfoncée mais timer pas fini
			result = false;
			timerRight--;
		}else{
			// premier appui, on init le timer
			timerRight = MAX_TIMER;
		}
		return result;
	}
	
	/**
	 * 
	 * @return vrai si la touche echap ou start est pressée (seulement une fois toutes les XX frames)
	 */
	public boolean isKeyEscapeJustPressedSinceTimer(){
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.ESCAPE);
		if(!result && controller360 != null){
			result = controller360.getButton(XBox360Pad.BUTTON_START);
		}
		
		if(!result){
			// touche non enfoncée, on remet le timer à 0
			timerStart = 0;
			
		}else if(timerStart > 0){
			// touche deja enfoncée mais timer pas fini
			result = false;
			timerStart--;
		}else{
			// premier appui, on init le timer
			timerStart = MAX_TIMER;
		}
		return result;
	}

	/**
	 * 
	 * @return vrai si la touche I ou select est pressée (seulement une fois toutes les XX frames)
	 */
	public boolean isKeyInfoJustPressedSinceTimer() {
		boolean result = false;
		result = Gdx.input.isKeyPressed(Keys.I);
		if(!result && controller360 != null){
			result = controller360.getButton(XBox360Pad.BUTTON_BACK);
		}
		
		if(!result){
			// touche non enfoncée, on remet le timer à 0
			timerSelect = 0;
			
		}else if(timerSelect > 0){
			// touche deja enfoncée mais timer pas fini
			result = false;
			timerSelect--;
		}else{
			// premier appui, on init le timer
			timerSelect = MAX_TIMER;
		}
		return result;
	}
	
	
	

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	 @Override
	    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
	        if(pointer < 5 && GameConstant.ANDROID){
	        	Vector3 v = new Vector3(screenX, screenY, 0);
	        	shared.cameraHud.unproject(v);
	            touches.get(pointer).touchX = v.x;
	            touches.get(pointer).touchY = v.y;
	            touches.get(pointer).touched = true;

	        }
	        return true;
	    }

	    @Override
	    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			
	        if(pointer < 5 &&  GameConstant.ANDROID){
	            touches.get(pointer).touchX = 0;
	            touches.get(pointer).touchY = 0;
	            touches.get(pointer).touched = false;
	        }
	        return true;
	    }


	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
//		if(touches.get(pointer) != null){
//			touches.get(pointer).touchX =screenX; 
//			touches.get(pointer).touchY =screenY; 
//		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	private boolean isTouch(float x, float y, float size){
		if(GameConstant.ANDROID && !touches.isEmpty()){
			  for(int i = 0; i < MAX_TOUCH_POINT; i++){
				  if(isTouch(touches.get(i),x, y, size)){
					  return true;
				  }
			  }
		}
		return false;
	}

	
	private boolean isTouch(TouchInfo touch,float x, float y, float size){
		if(touch.touched && touch.touchX > x && touch.touchX < (x+size) 
				&& touch.touchY > y && touch.touchY < (y+size)){
			return true;
		}
		return false;
	}
}
