package player;

public class PlayerStat {

	private int hunger, maxHunger;
	private int thirst, maxThirst;
	private int life, maxLife;
	
	
	public PlayerStat(int hunger, int thirst, int life){
		this.hunger = hunger;
		this.maxHunger = hunger;
		
		this.thirst = thirst;
		this.maxThirst = thirst;
		
		this.life = life;
		this.maxLife = life;
	}
	
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		this.hunger = hunger;
		if(this.hunger<0){
			this.hunger=0;
			System.out.println("ALERTE - FAIM NEGATIVE");
		}
	}
	public int getThirst() {
		return thirst;
	}
	public void setThirst(int thirst) {
		this.thirst = thirst;
		if(this.thirst<0){
			this.thirst=0;
			System.out.println("ALERTE - SOIF NEGATIVE");
		}
	}
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
		if(this.life<0){
			this.life=0;
			System.out.println("ALERTE - VIE NEGATIVE");
		}
	}

	public int getMaxHunger() {
		return maxHunger;
	}

	public void setMaxHunger(int maxHunger) {
		this.maxHunger = maxHunger;
	}

	public int getMaxThirst() {
		return maxThirst;
	}

	public void setMaxThirst(int maxThirst) {
		this.maxThirst = maxThirst;
	}

	public int getMaxLife() {
		return maxLife;
	}

	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}
	
}
