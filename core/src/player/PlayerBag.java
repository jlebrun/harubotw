package player;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;

public class PlayerBag {

	protected  HashMap<String, Integer> items;
	
	public PlayerBag(){
		items = new HashMap<String, Integer>();
	}
	
	public HashMap<String, Integer> getItems(){
		return items;
	}
	
	public void addItem(String itemCode){
		if(items.get(itemCode)==null){
			items.put(itemCode, Integer.valueOf(1));
		}else{
			items.put(itemCode, items.get(itemCode)+1);
		}
	}
	
	public int getQuantity(String itemCode){
		if(items.get(itemCode)==null){
			return 0;
		}else{
			return items.get(itemCode);
		}
	}
	
	public void removeItem(String itemCode){
		if(items.get(itemCode)==null || items.get(itemCode).intValue()<1){
			Gdx.app.log("ERROR", "no item");
		}else{
			if(items.get(itemCode).intValue()==1){
				items.remove(itemCode);
			}else{
				items.put(itemCode, items.get(itemCode)-1);
			}
		}
	}
	
	
}
