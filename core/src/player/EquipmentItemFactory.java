package player;

import shared.GameItem;
import shared.GameTexture;

public class EquipmentItemFactory {

	private static EquipmentItemFactory INSTANCE = new EquipmentItemFactory();
	public static enum EQUIPMENT_TYPE {BODY,HEAD, FEET, LEG};
	
	private EquipmentItemFactory(){
		
	}
	
	public static EquipmentItemFactory getInstance(){
		return INSTANCE;
	}
	
	/**
	 * Creer un equipement
	 * @param code
	 * @return
	 */
	public EquipmentItem createEquipment(String code){
		EquipmentItem item = null;
		if(code.equals(GameItem.EQUIP_LEG_PANTS_01)){
			item = new EquipmentItem(EQUIPMENT_TYPE.LEG, GameTexture.TXR_EQUIP_LEG_PANTS_01, "Pantalon basique");
		}
		
		return item;
	}
}
