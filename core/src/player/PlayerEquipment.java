package player;

import shared.SharedObject;

public class PlayerEquipment {
 	
	protected EquipmentItem leg = null;
	protected EquipmentItem body = null;
	protected EquipmentItem head = null;
	protected EquipmentItem feet = null;
	
	protected float xx;
	protected float yy;
	protected int caseSpriteX;
	protected int caseSpriteY;
	
	
	/**
	 * Dessine les equipements du heros
	 * @param shared
	 */
	public void render(SharedObject shared, int caseSpriteX, int caseSpriteY, float xx, float yy){
		this.xx = xx;
		this.yy = yy;
		this.caseSpriteX = caseSpriteX;
		this.caseSpriteY = caseSpriteY;
		this.renderEquipmentItem(feet,shared);
		this.renderEquipmentItem(leg,shared);
		this.renderEquipmentItem(body,shared);
		this.renderEquipmentItem(head,shared);
	}
	
	/**
	 * Dessine l'equipement
	 * @param item
	 * @param shared
	 * @param statetime
	 */
	private void renderEquipmentItem(EquipmentItem item, SharedObject shared){
		if(item != null){
			shared.spriteBatch.draw(item.getFrame(caseSpriteX,caseSpriteY),xx,yy);
		}
	}
	
	/**
	 * Met a jour les equipements du heros
	 * @param shared
	 */
	public void update(SharedObject shared){
		
	}
	
	
	

	public EquipmentItem getLeg() {
		return leg;
	}

	public void setLeg(EquipmentItem leg) {
		this.leg = leg;
	}

	public EquipmentItem getBody() {
		return body;
	}

	public void setBody(EquipmentItem body) {
		this.body = body;
	}

	public EquipmentItem getHead() {
		return head;
	}

	public void setHead(EquipmentItem head) {
		this.head = head;
	}

	public EquipmentItem getFeet() {
		return feet;
	}

	public void setFeet(EquipmentItem feet) {
		this.feet = feet;
	}

	
}
