package player;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class EquipmentItem {

	protected String name;
	protected EquipmentItemFactory.EQUIPMENT_TYPE type;
	protected TextureRegion[][] frames;
	
	/**
	 * Constructeur
	 * @param type
	 * @param name
	 */
	public EquipmentItem(EquipmentItemFactory.EQUIPMENT_TYPE type, TextureRegion[][] frames, String name){
		this.type = type;
		this.name = name;
		this.frames = frames;
	}
	
	public TextureRegion getFrame(int i, int j){
		return frames[j][i];
	}
}
