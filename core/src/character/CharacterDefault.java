package character;

import java.util.ArrayList;
import java.util.Arrays;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import player.PlayerEquipment;

import java.util.List;

import shared.GameConstant;
import shared.SharedObject;
import utils.Point;

public abstract class CharacterDefault {

	//////////////////////////////////////////////////////////////////////
	// ATTRIBUTS														//
	//////////////////////////////////////////////////////////////////////
	
	protected Texture baseTexture;
	protected TextureRegion currentFrame;
	
	protected float x=-1;
	protected float y=-1;
	protected int xDecalBlock = 0;
	protected int yDecalBlock = 0;
	protected int width=0;
	protected int height=0;
	protected int widthBlock=0;
	protected int heightBlock=0;
	protected boolean move = false;
	protected List<Point> blockPoints;
	
	public enum DIRECTION {UP, DOWN, RIGHT, LEFT};
	public DIRECTION direction;
	
	protected PlayerEquipment equipment;
	protected float statetime = 0f;
	
	protected int caseSpriteX=0, caseSpriteY=0;

	
	//////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS														//
	//////////////////////////////////////////////////////////////////////
	
	protected CharacterDefault(){
		blockPoints = new ArrayList<Point>();
	}
	
	
	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////
	
	public void update(SharedObject shared){
		// met a jour l'equipement du personnage
		if(equipment != null){
			equipment.update(shared);
		}
	}
	
	/**
	 * Dessine le personnage
	 * @param shared
	 */
	public void render(SharedObject shared){
		if(currentFrame != null){
			
			float xx = shared.heros.getX()-x-(this.width/2);
			float yy = shared.heros.getY()-y-(this.height/2);
			
			shared.spriteBatch.draw(currentFrame,xx,yy);
			
			// dessine l'equipement du personnage
			if(equipment != null){
				equipment.render(shared,caseSpriteX, caseSpriteY, xx, yy);
			}
		}
	}
	
	public List<Point> getBlockPoints(){
		return getBlockPoints(0,0);
	}
	
	public List<Point> getBlockPoints(float dx, float dy){
		float xx = x+dx+xDecalBlock;
		float yy = y+dy+yDecalBlock;
		blockPoints.clear();
		
		blockPoints.add(new Point(xx,yy)); // milieu

		blockPoints.add(new Point(xx-(widthBlock/2),yy+(heightBlock/2))); //coin haut gauche de la hitbox
		blockPoints.add(new Point(xx+(widthBlock/2),yy+(heightBlock/2))); //coin droit gauche de la hitbox
		blockPoints.add(new Point(xx-(widthBlock/2),yy-(heightBlock/2))); //coin haut gauche de la hitbox
		blockPoints.add(new Point(xx+(widthBlock/2),yy-(heightBlock/2))); //coin droit gauche de la hitbox
		
		
		return blockPoints;
	}
	
	/**
	 * Deplace l'objet
	 * @param dx le deplacement en X
	 * @param dy le deplacement en Y
	 */
	public void translate(SharedObject shared,float dx, float dy){
		this.translate(shared,dx, dy, false);
	}
	
	/**
	 * Deplace l'objet
	 * @param dx le deplacement en X
	 * @param dy le deplacement en Y
	 * @param ghost si false, on test les collisions
	 */
	public void translate (SharedObject shared, float dx, float dy, boolean ghost){
		this.translate(shared,dx,dy,ghost,false, false);
	}
	
	/**
	 * Deplace l'objet
	 * @param dx le deplacement en X
	 * @param dy le deplacement en Y
	 * @param ghost si false, on test les collisions
	 * @param moveCamera si vrai, on bouge egalement la camera
	 */
	protected void translate(SharedObject shared, float dx, float dy, boolean ghost, boolean moveCamera, boolean updateMap){
		
		// Si on test les colisions
		if(!ghost){
			int moveX = 0;
			int moveY = 0;
			int cpt = 0;
			int multiply = 1;
			
			if(dx<0){
				multiply = -1;
			}
			while(cpt < Math.abs(dx)){
				cpt++;
				if(shared.currentMap.isBlocked(shared,getBlockPoints((cpt*multiply),0))){
					break;
				}else{
					moveX+=multiply;
				}
			}
			cpt=0; // RAZ du compteur
			if(dy<0){
				multiply = -1;
			}else{
			 multiply = 1;
			}
			while(cpt < Math.abs(dy)){
				cpt++;
				if(shared.currentMap.isBlocked(shared,getBlockPoints(0,(cpt*multiply)))){
					break;
				}else{
					moveY+=multiply;
				}
			}
			
			dx = moveX;
			dy = moveY;
		}
		
		// Deplacement du heros et de la camera
		if(dx != 0 || dy != 0){
			this.move = true;
			this.x += dx;
			this.y += dy;
			
			if(updateMap){
				shared.currentMap.startMapEvent(shared,new Point(x,y));
				shared.currentMap.updateObjects(shared);
			}
			if(moveCamera){
				shared.camera.translate(dx, dy);
			}
		}else{
			this.move = false;
		}
		
	}
	
	
	//////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS												//
	//////////////////////////////////////////////////////////////////////
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	
	/**
	 * Met a jour le mouvement
	 * @param b
	 */
	public void setMove(boolean b){
		this.move = b;
	}
	
	public boolean getMove(){
		return this.move;
	}
	
}
