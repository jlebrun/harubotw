package character;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

import player.EquipmentItemFactory;
import player.PlayerEquipment;
import shared.GameItem;
import shared.GameTexture;
import shared.SharedObject;
import utils.Point;

public class Heros extends CharacterDefault{

	
	protected Animation walkUpAnimation;
	protected Animation walkDownAnimation;
	protected Animation walkLeftAnimation;
	protected Animation walkRightAnimation;
	
	protected static final int CELL_WIDTH = 24;
	protected static final int CELL_HEIGHT = 44;
	
	

	
	//////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS														//
	//////////////////////////////////////////////////////////////////////
	
	public Heros(SharedObject shared){
		super();
		this.width=32;
		this.height=48;
		this.x = 600;
		this.y = 1032;
		this.yDecalBlock = -12;
		this.widthBlock=24;
		this.heightBlock=24;
		shared.camera.translate(x, y);
		this.direction = DIRECTION.DOWN;
		this.initRessources(shared);
		this.blockPoints = new ArrayList<Point>();
		this.equipment = new PlayerEquipment();
		
		// Ajout du pantalon pour test
		this.equipment.setLeg(EquipmentItemFactory.getInstance().createEquipment(GameItem.EQUIP_LEG_PANTS_01));
		
	}

	
	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////
	
	public void initRessources(SharedObject shared){
		this.baseTexture = new Texture(GameTexture.STR_HEROS_WALK);

		TextureRegion[][] tmp = TextureRegion.split(baseTexture, CELL_WIDTH, CELL_HEIGHT); 

		TextureRegion[] walkUpFrames = new TextureRegion[3];
		TextureRegion[] walkDownFrames = new TextureRegion[3];
		TextureRegion[] walkLeftFrames = new TextureRegion[3];
		TextureRegion[] walkRightFrames = new TextureRegion[3];
		for (int i = 0; i < 3; i++) {
			walkDownFrames[i] = tmp[0][i];
			walkLeftFrames[i] = tmp[1][i];
			walkRightFrames[i] = tmp[2][i];
			walkUpFrames[i] = tmp[3][i];
		}
		this.walkDownAnimation = new Animation(0.1f, walkDownFrames);      
		this.walkLeftAnimation = new Animation(0.1f, walkLeftFrames);      
		this.walkRightAnimation = new Animation(0.1f, walkRightFrames);      
		this.walkUpAnimation = new Animation(0.1f, walkUpFrames);
		
		this.walkDownAnimation.setPlayMode(PlayMode.LOOP);
		this.walkLeftAnimation.setPlayMode(PlayMode.LOOP); 
		this.walkRightAnimation.setPlayMode(PlayMode.LOOP);  
		this.walkUpAnimation.setPlayMode(PlayMode.LOOP);
		
	}
	
	/**
	 * Dessine le heros a l'ecran
	 * @param shared : les objets partag�s
	 */
	public void render(SharedObject shared){

		if(direction == DIRECTION.UP){
			currentFrame = this.walkUpAnimation.getKeyFrame(statetime);
			this.caseSpriteY=3;
			this.caseSpriteX = this.walkUpAnimation.getKeyFrameIndex(statetime);
		}else if(direction == DIRECTION.DOWN){
			currentFrame = this.walkDownAnimation.getKeyFrame(statetime);
			this.caseSpriteX = this.walkDownAnimation.getKeyFrameIndex(statetime);
			this.caseSpriteY=0;
		}else if(direction == DIRECTION.LEFT){
			currentFrame = this.walkLeftAnimation.getKeyFrame(statetime);
			this.caseSpriteX = this.walkLeftAnimation.getKeyFrameIndex(statetime);
			this.caseSpriteY=1;
		}else if(direction == DIRECTION.RIGHT){
			currentFrame = this.walkRightAnimation.getKeyFrame(statetime);
			this.caseSpriteX = this.walkRightAnimation.getKeyFrameIndex(statetime);
			this.caseSpriteY=2;
		}
		
		// Dessine le heros
		super.render(shared);
		
		// Si mouvement : on met a jour le statetime
		if(move){
			statetime += Gdx.graphics.getDeltaTime(); 
		}
	}
	
	/**
	 * Deplace le personnage
	 */
	public void translate(SharedObject shared,float dx, float dy){
		this.move = true;
		if(dx > 0){
			this.direction = DIRECTION.RIGHT;
		}else if(dx < 0){
			this.direction = DIRECTION.LEFT;
		}else if(dy > 0){
			this.direction = DIRECTION.UP;
		}else if(dy < 0){
			this.direction = DIRECTION.DOWN;
		}
		super.translate(shared,dx, dy,false, true, true);
	}
	
	/**
	 * Genere et retourne la liste des points de collision
	 * @return liste des points de collision
	 */
	public List<Point> getBlockPoints(float dx, float dy){
		float xx = x+dx+xDecalBlock;
		float yy = y+dy+yDecalBlock;
		blockPoints.clear();
		
		blockPoints.add(new Point(xx,yy)); // milieu

		blockPoints.add(new Point(xx-(widthBlock/2),yy+(heightBlock/2))); //coin haut gauche de la hitbox
		blockPoints.add(new Point(xx+(widthBlock/2),yy+(heightBlock/2))); //coin droit gauche de la hitbox
		blockPoints.add(new Point(xx-(widthBlock/2),yy-(heightBlock/2))); //coin haut gauche de la hitbox
		blockPoints.add(new Point(xx+(widthBlock/2),yy-(heightBlock/2))); //coin droit gauche de la hitbox
		
		blockPoints.add(new Point(xx,yy+(heightBlock/2))); 
		blockPoints.add(new Point(xx,yy-(heightBlock/2))); 
		blockPoints.add(new Point(xx+(widthBlock/2),yy));
		blockPoints.add(new Point(xx-(widthBlock/2),yy)); 
		
		return blockPoints;
	}
	
	
}
