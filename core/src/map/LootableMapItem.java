package map;


import com.badlogic.gdx.graphics.g2d.TextureRegion;

import shared.GameConstant;
import shared.ItemProperty;
import shared.SharedObject;

public class LootableMapItem extends GameMapItem{
	
	//////////////////////////////////////////////////////////////////////
	// ATTRIBUTS														//
	//////////////////////////////////////////////////////////////////////
	private ItemProperty item;
	private TextureRegion textureRegion ;
	private boolean taken = false;
	
	//////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS														//
	//////////////////////////////////////////////////////////////////////
	
	public LootableMapItem(ItemProperty item, int x, int y){
		this.x = x;
		this.y = y;
		this.width = GameConstant.CELL_WIDTH;
		this.height = GameConstant.CELL_WIDTH;
		this.item = item;
		textureRegion = item.getMapTextureRegion((int)(Math.random()*4));
	}
	
	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////
	

	public void render(SharedObject shared){
		if(textureRegion != null && active && !taken){
			shared.spriteBatch.draw(textureRegion,renderX,renderY);
		}
	}
	
	public void update(SharedObject shared){
		if(active && !taken){
			if(shared.currentMap.getLootableItemOnContact() == null){
				if(this.herosOnMapObject(shared)){
					shared.currentMap.setLootableItemOnContact(this);
				}
			}else{
				if(this.equals(shared.currentMap.getLootableItemOnContact())){
					if(!this.herosOnMapObject(shared)){
						shared.currentMap.setLootableItemOnContact(null);
					}
					if(shared.input.isKeyAction1JustPressed()){
						this.taken = true;
						shared.currentMap.setLootableItemOnContact(null);
						shared.bag.addItem(item.getCode());
						// todo appel HUD pour afficher le message
					}
				}
			}
		}
	}
	
	


	public ItemProperty getItem() {
		return item;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LootableMapItem other = (LootableMapItem) obj;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item)){
			return false;
		}else if(this.x != other.getX()){
			return false;
		}else if(this.y != other.getY()){
			return false;
		}
		return true;
	}
	
	
	
}
