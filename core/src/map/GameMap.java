package map;

import java.util.List;

import com.badlogic.gdx.graphics.OrthographicCamera;

import shared.SharedObject;
import utils.Point;


public interface GameMap {
	public void renderBack(SharedObject shared,OrthographicCamera camera);
	public void renderFront(SharedObject shared,OrthographicCamera camera);
	public void renderObjects(SharedObject shared);
	public boolean isBlocked(SharedObject shared,List<Point> points);
	public void startMapEvent(SharedObject shared,Point p);
	public void updateObjects(SharedObject shared);
	public void updateMap(SharedObject shared);
	public LootableMapItem getLootableItemOnContact();
	public void setLootableItemOnContact(LootableMapItem lootableItemOnContact);
	public void init(SharedObject shared,String mapFileName);

}
