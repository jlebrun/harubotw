package map;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import shared.GameConstant;
import shared.GameItem;
import shared.SharedObject;
import view.GameCamera;
import utils.Point;

public  class GameMapImpl implements GameMap{

	//////////////////////////////////////////////////////////////////////
	// ATTRIBUTS														//
	//////////////////////////////////////////////////////////////////////

	private TiledMap map;
	private String mapFileName;
	private String name;
	private static TmxMapLoader tmxLoader = new TmxMapLoader();
	private int[] layersBack;
	private int[] layersFront;
	private int[] layersDebug;
	private int animatedObjectLayer;
	private List<AnimatedItem> animatedItems;
	private List<LootableMapItem> lootableItems;
	private List<GameMapItem> gameMapObjects;
	private List<TeleportItem> teleportItems;
	private LootableMapItem lootableItemOnContact = null;
	

	private int mapWidth;
	private int mapHeight;
	private int tilePixelWidth;
	private int tilePixelHeight;

	private int mapPixelWidth;
	private int mapPixelHeight;
	

	//////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS														//
	//////////////////////////////////////////////////////////////////////

	/**
	 * Constructeur
	 * @param shared
	 * @param mapFileName
	 * @param name
	 */
	public GameMapImpl(SharedObject shared, String mapFileName){
		this.init(shared, mapFileName);
	}
	
	/**
	 * Initialise une nouvelle carte
	 * @param shared
	 * @param mapFileName
	 * @param name
	 */
	public void init(SharedObject shared,String mapFileName){
		this.mapFileName = mapFileName;
		this.name = mapFileName;
		this.load(shared);
		
		
	}


	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////

	/**
	 * Charge les objets de la map
	 */
	public void load(SharedObject shared){
		map =  tmxLoader.load("maps/"+mapFileName);
		shared.renderer = new OrthogonalTiledMapRenderer(map);
		
		MapProperties prop = map.getProperties();
		this.mapWidth = prop.get("width", Integer.class);
		this.mapHeight = prop.get("height", Integer.class);
		this.tilePixelWidth = prop.get("tilewidth", Integer.class);
		this.tilePixelHeight = prop.get("tileheight", Integer.class);

		this.mapPixelWidth = mapWidth * tilePixelWidth;
		this.mapPixelHeight = mapHeight * tilePixelHeight;
		
		initBackLayersIndex(shared);
		initFrontLayersIndex(shared);
		initDebugLayersIndex(shared);
		
		initMapItems();
		initMapObject();
		updateObjects(shared);
		
		
	}

	/**
	 * Initialise les objets anim�s affich�s a l'ecran
	 */
	public void initMapItems(){
		animatedItems = new ArrayList<AnimatedItem>();
		lootableItems = new ArrayList<LootableMapItem>();
		teleportItems = new ArrayList<TeleportItem>();
		
		animatedObjectLayer = 8;
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(animatedObjectLayer);
		Cell cell;
		String property;
		
		for(int i=0;i<layer.getWidth();i++){
			for(int j=0;j<layer.getHeight();j++){
				cell = layer.getCell(i, j);
				if(cell != null && cell.getTile() != null){
					property =  (String) cell.getTile().getProperties().get("animatedObject");
					if(property != null){
						this.animatedItems.add(new AnimatedItem(property,i*GameConstant.CELL_WIDTH,j*GameConstant.CELL_WIDTH));
					}else{
						property =  (String) cell.getTile().getProperties().get("mapItem");
						if(property != null){
							this.lootableItems.add(new LootableMapItem(GameItem.ITEMS_PROPERTIES.get(property),i*GameConstant.CELL_WIDTH,j*GameConstant.CELL_WIDTH));
						}else{
							property =  (String) cell.getTile().getProperties().get("teleport");
							if(property != null){
								String map = (String) cell.getTile().getProperties().get("map");
								String direction = (String) cell.getTile().getProperties().get("direction");
								int mapX = Integer.valueOf((String) cell.getTile().getProperties().get("mapX"));
								int mapY = Integer.valueOf((String) cell.getTile().getProperties().get("mapY"));
								String doorStr = (String) cell.getTile().getProperties().get("door");
								int door = -1;
								if(doorStr != null){
									door = Integer.valueOf(doorStr);
								}
								this.teleportItems.add(new TeleportItem(i*GameConstant.CELL_WIDTH,(j*GameConstant.CELL_WIDTH),map,direction, mapX, mapY,door));
							}
						}
					}
				}
			}
		}
	}
	
	public void initMapObject(){
		// Test sur le layer des objets
		MapLayer layerObject = map.getLayers().get(9);
		MapObjects mapObjects = layerObject.getObjects();
		int x=-1;
		int y=-1;
		int width=0;
		int height =0;
		gameMapObjects = new ArrayList<GameMapItem>();
		
		// Test sur la couche objects
		if(mapObjects != null){
			for(MapObject obj : mapObjects){
				x = Math.round((Float) obj.getProperties().get("x"));
				y = Math.round((Float) obj.getProperties().get("y"));
				width = Math.round((Float) obj.getProperties().get("width"));
				height = Math.round((Float) obj.getProperties().get("height"));
				
				if(obj.getName()!=null && obj.getName().equals("dialbox")){
					String content =(String)obj.getProperties().get("txtFr");
					DialboxMapObject db = new DialboxMapObject(x, y, width, height, content);
					gameMapObjects.add(db);
				}
			}
		}
	}

	
	/**
	 * Initialise les couches basses de la map
	 */
	public void initBackLayersIndex(SharedObject shared){
		if(shared.HEROS_ON_BRIDGE){
			layersBack = new int[4];
			layersBack[0] = 0;
			layersBack[1] = 1;
			layersBack[2] = 2;
			layersBack[3] = 3;
		}else{
			layersBack = new int[3];
			layersBack[0] = 0;
			layersBack[1] = 1;
			layersBack[2] = 2;

		}

	}

	/**
	 * Initialise les couches hautes de la map
	 */
	public void initFrontLayersIndex(SharedObject shared){
		if(shared.HEROS_ON_BRIDGE){
			layersFront = new int[3];
			layersFront[0] = 4;
			layersFront[1] = 5;
			layersFront[2] = 6;
		}else{
			layersFront = new int[4];
			layersFront[0] = 3;
			layersFront[1] = 4;
			layersFront[2] = 5;
			layersFront[3] = 6;
		}

	}
	/**
	 * Initialise les couches debug de la map
	 */
	public void initDebugLayersIndex(SharedObject shared){
		layersDebug = new int[1];
		layersDebug[0] = 7;
	}

	/**
	 * @return les couches objets de mapping
	 */
	public int getBlockLayersIndex(){
		return 7;
	}

	/**
	 * Dessine le fond de la carte
	 */
	public void renderBack(SharedObject shared,OrthographicCamera camera){
		shared.renderer.setView(camera);
		shared.renderer.render(layersBack);
	}
	
	/**
	 * Dessine les objets
	 */
	public void renderObjects(SharedObject shared){
		if(animatedItems != null){
			for(AnimatedItem o : this.animatedItems){
				o.render(shared);
			}
		}
		if(lootableItems != null){
			for(LootableMapItem o : this.lootableItems){
				o.render(shared);
			}
		}
		if(teleportItems != null){
			for(TeleportItem o : this.teleportItems){
				o.render(shared);
			}
		}
	}
	
	/**
	 * Dessine le dernier niveau de la carte
	 */
	public void renderFront(SharedObject shared,OrthographicCamera camera){
		shared.renderer.render(layersFront);
		if(shared.DEBUG_MAP){
			shared.renderer.render(layersDebug);
		}
	}
	
	/**
	 * Met a jour les objets apr�s un mouvement (active/desactive)
	 */
	public void updateObjects(SharedObject shared){
		if(animatedItems != null){
			for(AnimatedItem o : this.animatedItems){
				o.updateAfterMove(shared);
			}
		}
		if(lootableItems != null){
			for(LootableMapItem o : this.lootableItems){
				o.updateAfterMove(shared);
			}
		}
		if(teleportItems != null){
			for(TeleportItem o : this.teleportItems){
				o.updateAfterMove(shared);
			}
		}
	}
	
	/**
	 * Met a jour les objets sur la carte
	 */
	public void updateMap(SharedObject shared){
		if(gameMapObjects != null){
			for(GameMapItem go : gameMapObjects){
				go.update(shared);
			}
		}
		if(lootableItems != null){
			for(LootableMapItem o : this.lootableItems){
				o.update(shared);
			}
		}
		if(teleportItems != null){
			for(TeleportItem o : this.teleportItems){
				o.update(shared);
			}
		}
	}

	/**
	 * Regarde si un evenement doit �tre declench� si l'objet est sur le point p et si oui, le declenche
	 * @param p le point de declenchement
	 */
	public void startMapEvent(SharedObject shared,Point p){
		
		// Test sur la couche "block"
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(getBlockLayersIndex());
		Cell cell = layer.getCell(((int)(p.getX()/GameConstant.CELL_WIDTH)), ((int)(p.getY()/GameConstant.CELL_WIDTH)) );
		if(cell != null && cell.getTile() != null){
			String property =  ""+(String) cell.getTile().getProperties().get("action");
			if(property.equals("goToBridge")){
				shared.HEROS_ON_BRIDGE = true;
				this.initBackLayersIndex(shared);
				this.initFrontLayersIndex(shared);
			}else if(property.equals("goToGround")){
				shared.HEROS_ON_BRIDGE = false;
				this.initBackLayersIndex(shared);
				this.initFrontLayersIndex(shared);
			}
		}
	}


	/**
	 * Retourne vrai si au moins un des points pass� en param�tre se trouve dans une zone bloqu�e
	 */
	public boolean isBlocked(SharedObject shared,List<Point> points){
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(getBlockLayersIndex());
		Cell cell;

		if(points != null && !points.isEmpty() && map != null){
			for(Point p : points){
				cell = layer.getCell(((int)(p.getX()/GameConstant.CELL_WIDTH)), ((int)(p.getY()/GameConstant.CELL_WIDTH)) );
				if(cell != null && cell.getTile() != null){
					String property =  ""+(String) cell.getTile().getProperties().get("block");

					if(property.equals("full")){
						return true;
					}else{
						int cellX = ((int)p.getX())%GameConstant.CELL_WIDTH;
						int cellY = ((int)p.getY())%GameConstant.CELL_WIDTH;

						if(property.equals("top_left")){
							if(cellX < (GameConstant.CELL_WIDTH/2) || cellY > (GameConstant.CELL_WIDTH/2)){
								return true;
							}
						}else if(property.equals("top")){
							if(cellY > (GameConstant.CELL_WIDTH/2)){
								return true;
							}

						}else if(property.equals("top_right")){
							if(cellX > (GameConstant.CELL_WIDTH/2) || cellY > (GameConstant.CELL_WIDTH/2)){
								return true;
							}
						}else if(property.equals("left")){
							if(cellX < (GameConstant.CELL_WIDTH/2)){
								return true;
							}
						}else if(property.equals("right")){
							if(cellX > (GameConstant.CELL_WIDTH/2)){
								return true;
							}
						}else if(property.equals("bot_left")){
							if(cellX < (GameConstant.CELL_WIDTH/2) || cellY < (GameConstant.CELL_WIDTH/2)){
								return true;
							}
						}else if(property.equals("bot")){
							if(cellY < (GameConstant.CELL_WIDTH/2)){
								return true;
							}
						}else if(property.equals("bot_right")){
							if(cellX > (GameConstant.CELL_WIDTH/2) || cellY < (GameConstant.CELL_WIDTH/2)){
								return true;
							}
						}else if(property.equals("center")){
							if((cellX > (GameConstant.CELL_WIDTH/4) && cellX < (3*GameConstant.CELL_WIDTH/4)) 
									&& (cellY > (GameConstant.CELL_WIDTH/4) && cellY < (3*GameConstant.CELL_WIDTH/4)) ){
								return true;
							}
						}else if(property.equals("top_left_corner")){
							if(cellX < (GameConstant.CELL_WIDTH/4) && cellY > (3*GameConstant.CELL_WIDTH/4) ){
								return true;
							}
						}else if(property.equals("top_right_corner")){
							if(cellX < (3*GameConstant.CELL_WIDTH/4) && cellY > (3*GameConstant.CELL_WIDTH/4) ){
								return true;
							}
						}else if(property.equals("bot_left_corner")){
							if(cellX < (GameConstant.CELL_WIDTH/4) && cellY < (GameConstant.CELL_WIDTH/4) ){
								return true;
							}
						}else if(property.equals("bop_right_corner")){
							if(cellX < (3*GameConstant.CELL_WIDTH/4) && cellY < (GameConstant.CELL_WIDTH/4) ){
								return true;
							}
						}else if(property.equals("top_left_bot_right")){
							if(cellX < (GameConstant.CELL_WIDTH/4) && cellY > (3*GameConstant.CELL_WIDTH/4) ){
								return true;
							}
							if(cellX < (3*GameConstant.CELL_WIDTH/4) && cellY < (GameConstant.CELL_WIDTH/4) ){
								return true;
							}
						}else if(property.equals("top_right_bot_left")){
							if(cellX < (3*GameConstant.CELL_WIDTH/4) && cellY > (3*GameConstant.CELL_WIDTH/4) ){
								return true;
							}
							if(cellX < (GameConstant.CELL_WIDTH/4) && cellY < (GameConstant.CELL_WIDTH/4) ){
								return true;
							}
						}else if(property.equals("ground") && !shared.HEROS_ON_BRIDGE ){
							return true;
						}else if(property.equals("bridge") && shared.HEROS_ON_BRIDGE){
							return true;
						}
					}
				}
			}
		}
		return false;
	}


	public LootableMapItem getLootableItemOnContact() {
		return lootableItemOnContact;
	}


	public void setLootableItemOnContact(LootableMapItem lootableItemOnContact) {
		this.lootableItemOnContact = lootableItemOnContact;
	}

	

}
