package map;


import shared.GameConstant;
import shared.SharedObject;

public class DialboxMapObject extends GameMapItem{

	private String[] contents;
	private int txtWidth;
	private int txtHeight;
	
	//////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS														//
	//////////////////////////////////////////////////////////////////////
	public DialboxMapObject(int x, int y, int width, int height, String content){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		initWidhtAndHeight(content);
	}


	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////
	
	/**
	 * Initialise la taille de la dialbox
	 * @param content
	 */
	private void initWidhtAndHeight(String content){
		this.contents = content.split("bbrr");
		
		int maxLength = 0;
		for(String s : contents){
			if(maxLength < s.length()){
				maxLength = s.length();
			}
		}
		this.txtWidth = 16 + maxLength*8;
		this.txtHeight= 16+contents.length*14;
	}
	
	/**
	 * Met a jour l'objet
	 */
	public void update(SharedObject shared){
		if(!shared.dialbox.isActive() && this.herosOnMapObject(shared) && shared.input.isKeyAction1JustPressed()){
			
			renderX = (GameConstant.WIDTH/2)+x-shared.heros.getX()+(GameConstant.CELL_WIDTH);
			renderY = (GameConstant.HEIGHT/2)+y-shared.heros.getY()+(GameConstant.CELL_WIDTH/2);
			shared.dialbox.initDialbox(shared,renderX, renderY,txtWidth,txtHeight, contents);
		}
	}
}
