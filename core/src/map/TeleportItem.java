package map;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

import shared.GameConstant;
import shared.GameTexture;
import shared.SharedObject;

public class TeleportItem extends GameMapItem{

	private String map;
	private String direction;
	private int mapX, mapY;
	private int doorCase;
	private Animation doorAnimation;
	private float stateTime = 0f;
	private boolean renderDoor = false;
	
	/**
	 * Constructeur
	 * @param x
	 * @param y
	 * @param map
	 * @param direction
	 * @param mapX
	 * @param mapY
	 * @param door
	 */
	public TeleportItem(int x, int y, String map, String direction, int mapX, int mapY, int door){
		this.x = x;//-(GameConstant.CELL_WIDTH/2);
		this.y = y;
		this.mapX = mapX;
		this.mapY = mapY;
		this.width = GameConstant.CELL_WIDTH;
		this.height = GameConstant.CELL_WIDTH;
		this.map = map+".tmx";
		this.direction = direction;
		this.doorCase = door;
		
		if(doorCase >= 0){
			doorAnimation = GameTexture.loadAnimation(GameTexture.TXR_DOOR, 5, doorCase, 0.12f);
			doorAnimation.setPlayMode(PlayMode.NORMAL);
			renderDoor = false;
		}
		
	}
	
	/**
	 * Dessine le teleport
	 * @param shared
	 */
	public void render(SharedObject shared){
//		shared.spriteBatch.draw(doorAnimation.getKeyFrames()[0], 100, 100);
		if(renderDoor && doorAnimation != null){
			System.out.println("Dessine "+doorAnimation.getKeyFrameIndex(stateTime)+" x="+x+" y="+y);
			shared.spriteBatch.draw(doorAnimation.getKeyFrame(stateTime), renderX, renderY+GameConstant.CELL_WIDTH);
			stateTime += Gdx.graphics.getDeltaTime(); 
		}
	}
	
	/**
	 * Met a jour le teleport
	 */
	public void update(SharedObject shared){
		if(active){
			if(this.herosOnMapObject(shared)){
				if(shared.input.isKeyAction1JustPressed() && !renderDoor){
					if(doorCase < 0){
						this.teleport(shared);
					}else{
						renderDoor = true;
						stateTime=0f;
					}
				}
			}
			
			if(renderDoor && doorAnimation.isAnimationFinished(stateTime)){
				this.teleport(shared);
			}
				
		}
	}
	
	/**
	 * Teleport le heros vers la nouvelle map
	 * @param shared
	 */
	private void teleport(SharedObject shared){
		shared.currentMap.init(shared,map);
		
		shared.heros.setX((mapX*GameConstant.CELL_WIDTH)+16);
		shared.heros.setY(mapY*GameConstant.CELL_WIDTH);
		shared.camera.getCamera().position.x = (mapX*GameConstant.CELL_WIDTH)+16;
		shared.camera.getCamera().position.y = mapY*GameConstant.CELL_WIDTH;
		shared.camera.getCamera().update();
		
		shared.currentMap.updateObjects(shared);
	}
}
