package map;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import shared.GameConstant;
import shared.SharedObject;

public class AnimatedItem extends GameMapItem{
	
	//////////////////////////////////////////////////////////////////////
	// ATTRIBUTS														//
	//////////////////////////////////////////////////////////////////////
	protected int nbFrames = 1;
	protected int currentFrame = 0;
	private int cptFrame = 0;
	protected String name;
	private static HashMap<String,Texture> textures = new HashMap<String,Texture>();
	private TextureRegion textureRegion ;
	
	//////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS														//
	//////////////////////////////////////////////////////////////////////
	
	public AnimatedItem(String name, int x, int y){
		this.name = name;
		this.x = x;
		this.y = y;
		this.width = GameConstant.CELL_WIDTH;
		this.height = GameConstant.CELL_WIDTH;
		
		if(textures.get(name) == null){
			Texture texture = new Texture("mapItems/animated/"+name+".png");
			textures.put(name, texture);
		}
		textureRegion = new TextureRegion(textures.get(name), 0,0,GameConstant.CELL_WIDTH,GameConstant.CELL_WIDTH);
		this.nbFrames = (textures.get(name).getWidth()/32);
	}
	
	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////

	public void render(SharedObject shared){
		if(textureRegion != null && active){
			this.updateAnimation();
			shared.spriteBatch.draw(textureRegion,renderX,renderY);
		}
	}
	
	private void updateAnimation(){
		if(cptFrame>=GameConstant.FRAME_DURATION){
			cptFrame = 0;
			currentFrame++;
			if(currentFrame>=nbFrames){
				currentFrame=0;
			}
			textureRegion.setRegion(currentFrame*width, 0, width, height);
		}else{
			cptFrame++;
		}
	}
}
