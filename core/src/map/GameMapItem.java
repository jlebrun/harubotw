package map;

import shared.SharedObject;

public abstract class GameMapItem {
	
	//////////////////////////////////////////////////////////////////////
	// ATTRIBUTS														//
	//////////////////////////////////////////////////////////////////////
	protected float x = -1;
	protected float y = -1;
	protected int width = 0;
	protected int height = 0;
	protected float renderX = -1;
	protected float renderY = -1;
	protected boolean active;

	
	
	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////
	
	public boolean herosOnMapObject(SharedObject shared){
		if(shared.heros.getX()> x && shared.heros.getX()< (x+width) 
		&& shared.heros.getY()> y && shared.heros.getY()< (y+height)){
			return true;
		}
		
		return false;
	}
	
	public void updateAfterMove(SharedObject shared){
		renderX = x-shared.heros.getX();
		renderY = y-shared.heros.getY();
//		if( renderX>=-width  && renderY>=-height
//		&&  renderX<2*GameConstant.WIDTH  && renderY<2*GameConstant.HEIGHT ){
//			render = true;
//		}else{
//			render = false;
//		}
		active = true;
	}
	
	public void update(SharedObject shared){
		// ERREUR
	};
	
	//////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS												//
	//////////////////////////////////////////////////////////////////////
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
}
