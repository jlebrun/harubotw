package map;

import shared.SharedObject;

public class TreeMapItem extends InterractiveMapItem{

	public TreeMapItem(){
		super();
	}
	
	/**
	 * Dessine la zone sous le h�ros
	 */
	public void renderBot(SharedObject shared){
		super.renderBot(shared);
	}
	
	/**
	 * Dessine la zone par dessus le h�ros
	 */
	public void renderTop(SharedObject shared){
		super.renderTop(shared);
	}
	
	/**
	 * Met a jour l'arbre
	 */
	public void update(SharedObject shared){
		super.update(shared);
	}
}
