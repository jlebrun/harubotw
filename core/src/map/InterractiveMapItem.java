package map;

import shared.SharedObject;

public abstract class InterractiveMapItem extends GameMapItem{
	
	public void renderBot(SharedObject shared){
		
	}
	
	public void renderTop(SharedObject shared){
		
	}
	
	public void update(SharedObject shared){
		
	}

}
