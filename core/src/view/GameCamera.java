package view;

import com.badlogic.gdx.graphics.OrthographicCamera;

import shared.GameConstant;
import shared.SharedObject;


public class GameCamera {
	
	//////////////////////////////////////////////////////////////////////
	// ATTRIBUTS														//
	//////////////////////////////////////////////////////////////////////
	private OrthographicCamera camera;
	
	public GameCamera(){
		this.camera = new OrthographicCamera(GameConstant.WIDTH, GameConstant.HEIGHT);
	}
	
	public void render(SharedObject shared){
		
		shared.currentMap.renderBack(shared,camera);
		
		
		// Affichage de la map
		shared.spriteBatch.begin();
		shared.currentMap.renderObjects(shared);
		shared.heros.render(shared);
		shared.spriteBatch.end();
		
		shared.currentMap.renderFront(shared,camera);
		
		
		
		// Affichage du HUD
		shared.hudBatch.begin();
		shared.dialbox.render(shared);
		shared.hud.render(shared);
		shared.hudBatch.end();
		
		
	}
	
	public void translate(float dx, float dy){
		camera.translate(dx, dy);
		camera.update();
	}
	
	public OrthographicCamera getCamera(){
		return camera;
	}
}
