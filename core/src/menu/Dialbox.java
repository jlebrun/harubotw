package menu;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import shared.GameConstant;
import shared.GameTexture;
import shared.SharedObject;

public class Dialbox {

	//////////////////////////////////////////////////////////////////////
	// ATTRIBUTS														//
	//////////////////////////////////////////////////////////////////////
	private NinePatchDrawable ninePatch;
	private String[] contents = null;
	private boolean active = false;
	private float x = -1;
	private float y = -1;
	private int width = 0;
	private int height = 0;
	private int cpt = 0;
	
	//////////////////////////////////////////////////////////////////////
	// CONSTRUCTOR														//
	//////////////////////////////////////////////////////////////////////
	
	public Dialbox(){
		NinePatch patch = new NinePatch(GameTexture.TX_DIALBOX_9, 12, 12, 12, 12);
		ninePatch = new NinePatchDrawable(patch);
		contents = null;
	}
	
	//////////////////////////////////////////////////////////////////////
	// FUNCTIONS														//
	//////////////////////////////////////////////////////////////////////
	
	/**
	 * Initialise la dialbox
	 * @param shared
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param contents
	 */
	public void initDialbox(SharedObject shared,float x, float y,int width,int height, String[] contents){
		this.width = width;
		this.height = height;
		this.x = x-(width/2)-((GameConstant.CELL_WIDTH/2));
		this.y = y+(GameConstant.CELL_WIDTH)+(GameConstant.CELL_WIDTH/2);
		active = true;
		cpt = 30;
		this.contents = contents;
		shared.ON_MENU = true;
	}
	
	/**
	 * Ferme la dialbox
	 * @param shared
	 */
	public void closeDialbox(SharedObject shared){
		contents = null;
		active = false;
		shared.ON_MENU = false;
	}
	
	/**
	 * Met a jour la dialbox
	 * @param shared
	 */
	public void update(SharedObject shared){
		if(active ){
			if(cpt>0){
				cpt--;
			}else if(cpt==0 && shared.input.isKeyAction1Pressed()){
				this.closeDialbox(shared);
			}
		}
	}

	/**
	 * Dessine la dialbox
	 * @param shared
	 */
	public void render(SharedObject shared){
		if(active){
			ninePatch.draw(shared.hudBatch, x, y, width, height);
			shared.hudBatch.draw(GameTexture.TX_DIALBOX_ARROW,x+(width/2)-7,y-7);
			for(int i=0;i<contents.length;i++){
				shared.font.draw(shared.hudBatch, contents[i], x+8, y+height-8-(i*12));
			}
			
		}
	}
	
	//////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS												//
	//////////////////////////////////////////////////////////////////////
	public boolean isActive(){
		return active;
	}
	
}
