package menu;


import shared.GameTexture;
import shared.SharedObject;

public class Hud {

	/**
	 * 
	 */
	public Hud(){
		
	}
	
	/**
	 * Met a jour le HUD
	 * @param shared
	 */
	public void update(SharedObject shared){
		shared.daytime.update(shared);
	}
	
	/**
	 * Dessine le HUD
	 * @param shared
	 */
	public void render(SharedObject shared){
		
		// On affiche le jour/nuit
		shared.daytime.render(shared);

		// Affichage du loot
		this.drawLoot(shared);
		
		// Affichage des barres de vie/energi
		this.drawBars(shared);
		
		shared.font.draw(shared.hudBatch, "x="+shared.heros.getX()+" y="+shared.heros.getY(), 8, 16);
		shared.font.draw(shared.hudBatch, "camX="+shared.camera.getCamera().position.x +" camY="+shared.camera.getCamera().position.y, 8, 32);
		
	}
	
	/**
	 * 
	 * @param shared
	 */
	private void drawLoot(SharedObject shared){
		if(shared.currentMap.getLootableItemOnContact() != null){
			shared.font.setColor(0.1f, 0.1f, 0.1f, 1f);
			int xTxt = (310-(shared.currentMap.getLootableItemOnContact().getItem().getName().length()*6));
			shared.font.draw(shared.hudBatch, "Ramasser '"+shared.currentMap.getLootableItemOnContact().getItem().getName()+"' ?", xTxt,220);
			
			shared.font.setColor(0.9f, 0.9f, 0.9f, 1f);
			shared.font.draw(shared.hudBatch, "Ramasser '"+shared.currentMap.getLootableItemOnContact().getItem().getName()+"' ?", xTxt,221);

		}
	}
	
	/**
	 * 
	 * @param shared
	 */
	private void drawBars(SharedObject shared){
		shared.hudBatch.draw(GameTexture.TXR_HUD_ICONS[1][0], 4,284);
		shared.hudBatch.draw(GameTexture.TXR_HUD_ICONS[0][0], 4,308);
		for(int i=0;i<shared.herosStat.getLife();i++){
			shared.hudBatch.draw(GameTexture.TXR_HUD_ICONS[2][0], 4+(18*i),332);
		}
		shared.font.setColor(0.1f, 0.1f, 0.1f, 1f);
		shared.font.draw(shared.hudBatch, ((shared.herosStat.getHunger()*100)/shared.herosStat.getMaxHunger())+"%", 32, 326);
		shared.font.draw(shared.hudBatch, ((shared.herosStat.getThirst()*100)/shared.herosStat.getMaxThirst())+"%", 32, 302);
		
		shared.font.setColor(0.9f, 0.9f, 0.9f, 1f);
		shared.font.draw(shared.hudBatch, ((shared.herosStat.getHunger()*100)/shared.herosStat.getMaxHunger())+"%", 32, 327);
		shared.font.draw(shared.hudBatch, ((shared.herosStat.getThirst()*100)/shared.herosStat.getMaxThirst())+"%", 32, 303);
	}

}
