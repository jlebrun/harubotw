package com.jln.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import game.Launcher;
import shared.GameConstant;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = GameConstant.WIDTH;
		config.height = GameConstant.HEIGHT;
		GameConstant.ANDROID = false;
		config.backgroundFPS = 60;
		config.foregroundFPS = 60;
		config.title = GameConstant.TITLE ;
		new LwjglApplication(new Launcher(), config);
	}
}
